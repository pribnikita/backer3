<?php
header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  if (!empty($_GET['save'])) {
    print('Спасибо, результаты сохранены.');
  }
  include('form.php');
  exit();
}

$errors = FALSE;
$trimed=[];

foreach ($_POST as $key => $value)
	if (is_string($value))
		$trimed[$key] = trim($value);
	else
		$trimed[$key] = $value;

  if (empty($trimed['name'])) {
    print('Заполните имя.<br/>');
    $errors = TRUE;
  }
  if (!preg_match('/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/', $trimed['email'])) {
    print('Заполните email.<br/>');
    $errors = TRUE;
  }
  if (!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $trimed['bd'])) {
    print('Заполните дату рождения.<br/>');
    $errors = TRUE;
  }
  if (!preg_match('/^[MFO]$/', $trimed['sex'])) {
    print('Заполните пол.<br/>');
    $errors = TRUE;
  }
  if (!preg_match('/^[2-4]$/', $trimed['limbs'])) {
    print('Заполните количество конечностей.<br/>');
    $errors = TRUE;
  }

  if (!isset($trimed['contract'])) {
   print('Вы не ознакомились с контрактом.<br/>');
    $errors = TRUE;
  }


if ($errors) {
  exit();
}

$user = 'u20401';
$pass = '3661797';
$db = new PDO('mysql:host=localhost;dbname=u20401', $user, $pass, array(PDO::ATTR_PERSISTENT => true));


try {
  $stmt1 = $db->prepare("INSERT INTO form_data (name, email, bd, sex, limbs, bio ) VALUES (?, ?, ?, ?, ?, ?)");
  $stmt1 -> execute([$trimed['name'], $trimed['email'], $trimed['bd'], $trimed['sex'], $trimed['limbs'], $trimed['bio']]);
  $stmt2 = $db->prepare("INSERT INTO form_supers (id_user, id_sups ) VALUES (?, ?)");
  $lastId = $db->lastInsertId();
  foreach ($trimed['superpowers'] as $super)
    $stmt2 -> execute([$lastId, $super]);
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}

header('Location: ?save=1');
