<!DOCTYPE html>
<html lang="ru">


	<head> <!-- Техническая информация о документе -->
		<meta charset="UTF-8"> <!-- Определяем кодировку символов документа -->
		<title>P O M O G I T E</title> <!-- Задаем заголовок документа -->
		<link rel="stylesheet" href="style.css">
	</head>

	<body>
		<header><div>
  			<img src="https://i.imgur.com/UxHIhiw.png" width="100"  height = "100" style="float:left; margin-right: 10px" alt="Moon">
			P O M O G I T E</div>

</header>
<nav><a href="#links">Ссылки</a> | <a href="#table">Таблица</a> |<a href="#form">Форма</a></nav>
	<div id="main">
	
	<div id = "formcontent">
	<h2 id="form" >Форма</h2>

   		<form action="." method ="POST" class="style">

   			Ваше имя:<br>
      		<label >
        	<input name="name"
          	value="Мария Склодоовская-Кюри">
      		</label>
      		<br>

      		Ваш адрес электронной почты:<br>
      		<label >
        	<input name="email"
          	value="example@gmail.com"
          	type="email">
      		</label>
      		<br>

			Ваша дата рождения:<br>
      		<label>
        	<input name="bd"
          	value="1867-11-07"
          	type="date">
      		</label>
      		<br>

      		Ваша гендерная идентефикация:<br>
      		<label ><input type="radio" checked="checked"
        	name="sex" value="M" />
        	Мужчина</label>
      		<label><input type="radio"
        	name="sex" value="F" />
        	Женщина</label>
        	<label><input type="radio"
        	name="sex" value="O" />
        	Небинарная радиокнопка</label>
        	<br>

			Количество конечностей:<br>
      		<label ><input type="radio" checked="checked"
        	name="limbs" value="2" />
        	2</label>
      		<label><input type="radio"
        	name="limbs" value="3" />
        	3</label>
        	<label><input type="radio"
        	name="limbs" value="4" />
        	4</label>
        	<br>
			
			
			Сверхспособности:<br>
      		<label >
        	<select name="superpowers[]"
          	multiple="multiple">
         	<option value="1">Бессмертие</option>
          	<option value="2" selected="selected">Прохождение сквозь стены</option>
          	<option value="3" selected="selected">Левитация</option>
        	</select>
      		</label>
      		<br>

      		Биография:<br>
      		<label>
        	<textarea name="bio">Всё хорошо, что хорошо кончается</textarea>
      		</label>
      		<br>

      		<br>
      		<label><input type="checkbox" checked="checked"
        	name="contract" />
        	С контрактом ознакомлен</label>
        	<br>
 
      <input type="submit" value="Отправить" />
    </form>
	</div>
    </div>
   
    <footer> Мастерами кунг-фу - не рождаются. Мастерами кунг-фу - становятся </footer> 
	</body>

</html>
